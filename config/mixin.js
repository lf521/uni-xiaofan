import store from '@/store';

export default {
	methods: {
		onJump(url,type='navigateTo'){
			if(type=='tabbar'){
				uni.switchTab({
					url:url
				})
			}else{
				uni.navigateTo({
				    url:url
				})
			}
		},
		// 跳转前判断登录
		onTokenJump(url) {
			this.judgeLogin(() => {
				uni.navigateTo({
					url: url
				});
			});
		},
	},
	onShareAppMessage(res) {
		return {
			title: '',
			// #ifdef MP-TOUTIAO
			desc: '',
			// #endif
			imageUrl: '',
			path: '/pages/index/index'
		}
	},
	onShareTimeline(res) {
		return {
			title: '',
			imageUrl: '',
			path: '/pages/index/index'
		}
	}
}